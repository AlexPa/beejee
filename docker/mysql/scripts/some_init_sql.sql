CREATE TABLE IF NOT EXISTS `task`
(
    `id`
    INT
    PRIMARY
    KEY
    AUTO_INCREMENT,
    `name`
    VARCHAR
(
    255
) NOT NULL,
    `email` VARCHAR
(
    255
) NOT NULL,
    `text` TEXT NOT NULL,
    `status` int NOT NULL DEFAULT 0,
    `admin_edited` int NOT NULL DEFAULT 0,
    `create_date` TIMESTAMP NOT NULL
    );

CREATE TABLE IF NOT EXISTS `user`(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `email` VARCHAR(255) UNIQUE NOT NULL,
    `name` VARCHAR(255) UNIQUE NOT NULL,
    `password_hash` VARCHAR(255) NOT NULL,
    `create_date` TIMESTAMP NOT NULL
);
INSERT INTO `user` (`id`, `email`, `name`, `password_hash`, `create_date`) VALUES
(1,	'admin@yopmail.com',	'admin',	'$2y$10$2UQprc3HW1jq4YWy2jO3r.LWQet6Ro3yRUu3ABRCviRp//arYPVDm',	'2023-05-30 03:16:37'),
(2,	'user@yopmail.com',	'user',	'$2y$10$RdWhqMvaTkj4a8ewjG23hObiWajxSuPeOsCjnzKifYqIKrFZuCySG',	'2023-05-30 03:16:37');