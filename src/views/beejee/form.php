<form action="" method="post">
    <?php
    if ($isCreateScenario): ?>
        <div class="mb-3">
            <label for="formName" class="form-label">Имя</label>
            <input type="text" class="form-control" placeholder="Имя" id="formName" name="name" value="<?= $l->name ?>">
        </div>
        <div class="mb-3">
            <label for="formEmail" class="form-label">Email</label>
            <input type="text" class="form-control" placeholder="Email" id="formEmail" name="email"
                   value="<?= $l->email ?>">
        </div>
    <?php
    else: ?>
        <div class="form-check">
            <input class="form-check-input" type="hidden" name="status" value="<?= \models\Task::STATUS_NEW ?>">
            <input class="form-check-input" type="checkbox"
                   name="status" <?= ($l->status == \models\Task::STATUS_FINISHED) ? 'checked' : '' ?>
                   value="<?= \models\Task::STATUS_FINISHED ?>"
                   id="formStatus">
            <label class="form-check-label" for="formStatus">
                Задача выполнена
            </label>
        </div>
    <?php
    endif; ?>
    <div class="mb-3">
        <label for="formText" class="form-label">Текст</label>

        <textarea class="form-control" placeholder="Текст" id="formText" name="text"
        ><?= $l->text ?></textarea>
    </div>

    <div class="mb-3">
        <button type="submit" class="btn btn-dark"><?= $isCreateScenario ? 'Создать' : 'Обновить' ?></button>
    </div>
</form>