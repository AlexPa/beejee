<?php

$tasks = $data['tasks'];
$tasksCount = $data['tasksCount'];
$taskRequest = $data['taskRequest'];
?>
<h1>Список задач <a href="/task/create" class="btn btn-dark">Создать задачу</a></h1>

<?php
if (\app\App::getSession()->hasFlash(\app\Session::FLASH_SUCCESS)): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= \app\App::getSession()->getFlash(\app\Session::FLASH_SUCCESS) ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php
endif; ?>

<table class="table">
    <thead>
    <tr>
        <th scope="col"><a href="<?= \app\Helper::getSortLink(
                "/",
                "id",
                $taskRequest
            ) ?>">Id</a></th>
        <th scope="col"><a href="<?= \app\Helper::getSortLink(
                "/",
                "name",
                $taskRequest
            ) ?>">Имя</a></th>
        <th scope="col"><a href="<?= \app\Helper::getSortLink(
                "/",
                "email",
                $taskRequest
            ) ?>">Email</a></th>
        <th scope="col"><a href="<?= \app\Helper::getSortLink(
                "/",
                "status",
                $taskRequest
            ) ?>">Выполнено</a></th>
        <th scope="col">Отредактировано администратором</th>
        <th scope="col">Текст</th>
        <?php
        if (\models\User::isAdmin()): ?>
            <th scope="col"></th>
        <?php
        endif; ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($tasks as $task): ?>
        <tr>
            <th scope="row"><?= $task->id ?></th>
            <td><?= $task->name ?></td>
            <td><?= $task->email ?></td>
            <td><?= $task->status == \models\Task::STATUS_FINISHED ? 'да' : 'нет' ?></td>
            <td><?= $task->admin_edited == \models\Task::ADMIN_EDITED_YES ? 'да' : 'нет' ?></td>
            <td><?= $task->text ?></td>
            <?php
            if (\models\User::isAdmin()): ?>
                <td><a class="editTask" href="/task/update/<?= $task->id ?>">✏️</a></td>
            <?php
            endif; ?>
        </tr>
    <?php
    endforeach; ?>
    </tbody>
</table>

<div class="paginationBeeJee">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php
            $maxPage = ceil($tasksCount / $taskRequest->getLimit());

            for ($pageNum = 1; $pageNum <= $maxPage; $pageNum++):
                ?>
                <?php
                if ($taskRequest->getPage() == $pageNum): ?>
                    <li class="page-item active">
                        <span class="page-link"><?= $pageNum ?></span>
                    </li>
                <?php
                else: ?>
                    <li class="page-item">
                        <a class="page-link"
                           href="<?= \app\Helper::getPaginationLink(
                               "/",
                               $pageNum,
                               $taskRequest
                           ) ?>"><?= $pageNum ?></a></li>
                <?php
                endif; ?>
            <?php
            endfor; ?>
        </ul>
    </nav>
</div>