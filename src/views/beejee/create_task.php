<?php

/**
 * @var $l \app\formRequest\TaskRequest
 */
$l = $data['taskRequest'];
$task = $data['task'];
$isCreateScenario = $task === null;
?>
    <h1>Создать задачу</h1>
<?php
if (\app\App::getSession()->hasFlash(\app\Session::FLASH_ERROR)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= \app\App::getSession()->getFlash(\app\Session::FLASH_ERROR) ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php
endif; ?>
<?php
require "form.php" ?>