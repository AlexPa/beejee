<?php

/**
 * @var $l \app\formRequest\LoginRequest
 */
$l = $data['loginRequest'];
?>
<h1>Войти</h1>
<?php
if (\app\App::getSession()->hasFlash(\app\Session::FLASH_ERROR)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= \app\App::getSession()->getFlash(\app\Session::FLASH_ERROR) ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php
endif; ?>
<form action="/auth/login" id="loginForm" method="post">
    <div class="mb-3">
        <label for="formName" class="form-label">Логин</label>
        <input type="text" class="form-control" placeholder="Имя" id="formName" name="name" value="<?= $l->name ?>">
    </div>
    <div class=" mb-3">
        <label for="formPassword" class="form-label">Пароль</label>
        <input type="password" class="form-control" placeholder="Пароль" id="formPassword" name="password"
               value="<?= $l->password ?>">
    </div>
    <div class="mb-3">
        <button type="submit" class="btn btn-dark">Войти</button>
    </div>
</form>
