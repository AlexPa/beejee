<?php

namespace app\database;

use Kodols\MySQL\Library;
use Kodols\MySQL\Server;

final class Db
{
    private static Db|null $instance = null;

    private Server $server;

    /**
     * @param array $config
     * @return Db|null
     * @throws \Exception
     */
    public static function getInstance(array $config): Db
    {
        if (self::$instance === null) {
            self::$instance = new self();
            $KML = new Library();
            $cfg = $KML->newConfiguration();
            $cfg->setHostname($config['hostname']);
            $cfg->setDatabase($config['database']);
            $cfg->setUsername($config['username']);
            $cfg->setPassword($config['password']);
            $KML->attachConfiguration($cfg);
            self::$instance->server = $KML->connect();
        }
        return self::$instance;
    }

    /**
     * @return Server
     */
    public function getServer(): Server
    {
        return $this->server;
    }
}