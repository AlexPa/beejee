<?php

namespace app\formRequest;

interface ValidateInterface
{
    public function validate(): bool;

    public function getPropsForValidate(): array;

    public function getErrors(): array;
}