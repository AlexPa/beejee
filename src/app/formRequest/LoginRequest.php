<?php

namespace app\formRequest;

use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

class LoginRequest implements ValidateInterface
{
    public string $name;

    public string $password;

    private ?Validation $validation = null;

    public function validate(): bool
    {
        $validator = new Validator();
        $this->validation = $validator->make($this->getData(), [
            'name' => 'required',
            'password' => 'required|min:3',
        ]);
        $this->validation->validate();
        return !$this->validation->fails();
    }

    private function getData(): array
    {
        $ret = [];
        foreach ($this->getPropsForValidate() as $prop) {
            $ret[$prop] = $this->$prop;
        }
        return $ret;
    }

    public function getPropsForValidate(): array
    {
        return ['name', 'password'];
    }

    public function getErrors(): array
    {
        if ($this->validation == null) {
            return [];
        }
        return $this->validation->errors()->all();
    }
}