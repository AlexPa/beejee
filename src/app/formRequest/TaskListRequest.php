<?php

namespace app\formRequest;

use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

class TaskListRequest implements ValidateInterface
{
    public string $page = "1";
    public string $order = "id";
    private string $limit = "3";
    private string $offset = "0";
    private ?Validation $validation = null;
    private int $orderDirection = SORT_DESC;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getOrderDirection(): int
    {
        return $this->orderDirection;
    }

    /**
     * @return string
     */
    public function getLimit(): string
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): string
    {
        if (!$this->validate()) {
            return "0";
        }
        return ($this->page - 1) * $this->limit;
    }

    public function validate(): bool
    {
        $validator = new Validator();
        $this->validation = $validator->make($this->getData(), [
            'page' => 'required|numeric',
            'order' => [
                'required',
                $validator('in', ["name"])->strict()
            ]
        ]);
        $this->validation->validate();
        return !$this->validation->fails();
    }

    private function getData(): array
    {
        $ret = [];
        foreach ($this->getPropsForValidate() as $prop) {
            $ret[$prop] = $this->$prop;
        }
        return $ret;
    }

    public function getPropsForValidate(): array
    {
        return ['page', 'order'];
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    public function getErrors(): array
    {
        return [];
    }

    public function init(): void
    {
        if ($this->order == "") {
            $this->order = "id";
        }
        if ($this->page == "") {
            $this->page = "1";
        }
        $validator = new Validator();
        $validation = $validator->validate(['order' => $this->order], [
            'order' => [
                'required',
                $validator('in', ["id", "-id", "name", "-name", "email", "-email", "status", "-status"])
            ]
        ]);

        if ($validation->fails()) {
            $this->order = "id";
        }
        if (preg_match("([\\\-]+)", $this->order)) {
            $this->orderDirection = SORT_ASC;
            $this->order = str_replace("-", "", $this->order);
        } else {
            $this->orderDirection = SORT_DESC;
            $this->order = str_replace("-", "", $this->order);
        }
    }
}