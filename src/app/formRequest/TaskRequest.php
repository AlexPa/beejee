<?php

namespace app\formRequest;

use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

class TaskRequest implements ValidateInterface
{
    public string $name;

    public string $email;

    public string $text;

    public int $status;

    private ?Validation $validation = null;

    public function validate(): bool
    {
        foreach ($this->getPropsForValidate() as $prop) {
            $this->$prop = trim($this->$prop);
            $this->$prop = htmlentities($this->$prop, ENT_QUOTES, "UTF-8");
            $this->$prop = htmlspecialchars($this->$prop, ENT_QUOTES);
        }
        $validator = new Validator();
        $this->validation = $validator->make($this->getData(), [
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required|min:5',
            'status' => 'numeric'
        ]);
        $this->validation->validate();
        return !$this->validation->fails();
    }

    public function getPropsForValidate(): array
    {
        return ['name', 'email', 'text', 'status'];
    }

    private function getData(): array
    {
        $ret = [];
        foreach ($this->getPropsForValidate() as $prop) {
            $ret[$prop] = $this->$prop;
        }
        return $ret;
    }

    public function getErrors(): array
    {
        if ($this->validation == null) {
            return [];
        }
        return $this->validation->errors()->all();
    }
}