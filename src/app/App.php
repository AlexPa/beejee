<?php

namespace app;

use app\database\Db;
use controllers\AuthController;
use controllers\BeeJeeController;
use controllers\DefaultController;
use Pecee\Http\Middleware\Exceptions\TokenMismatchException;
use Pecee\SimpleRouter\Exceptions\HttpException;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;

class App
{
    public static string $name;
    private static Db $db;

    private static Session $session;

    /**
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        self::$db = Db::getInstance($config['components']['db']);
        self::$name = $config['name'];
        self::$session = new Session($config);
    }

    /**
     * @return Session
     */
    public static function getSession(): Session
    {
        return self::$session;
    }

    public static function getBaseDir(): string
    {
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
    }

    /**
     * @return Db
     */
    public static function getDb(): Db
    {
        return self::$db;
    }

    /**
     * @throws \Exception
     */
    public function run(): void
    {
        $this->handleRoutes();
    }

    /**
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws TokenMismatchException
     */
    private function handleRoutes(): void
    {
        Router::group(['middleware' => AdminMiddleware::class], function () {
            Router::match([Request::METHOD_GET, Request::METHOD_POST],
                '/task/update/{id}',
                [BeeJeeController::class, 'updateTask']);
        });

        Router::group(['middleware' => AuthMiddleware::class], function () {
            Router::get('/auth/logout', [AuthController::class, 'logout']);
        });


        Router::get('/', [BeeJeeController::class, 'index']);
        Router::match([Request::METHOD_GET, Request::METHOD_POST],
            '/task/create',
            [BeeJeeController::class, 'createTask']);
        Router::match([Request::METHOD_GET, Request::METHOD_POST], '/auth/login', [AuthController::class, 'login']);
        Router::get('/notfound', [DefaultController::class, 'notfound']);
        Router::get('/forbidden', [DefaultController::class, 'forbidden']);
        Router::error(function (\Pecee\Http\Request $request, \Exception $exception) {
            switch ($exception->getCode()) {
                case 404:
                    Router::response()->redirect('/notfound');
                case 403:
                    Router::response()->redirect('/forbidden');
            }
        });
        Router::start();
    }
}