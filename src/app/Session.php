<?php

namespace app;

use models\UserInterface;

class Session
{
    public const FLASH_ERROR = "flash_error";
    public const FLASH_SUCCESS = "flash_success";


    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        session_start();
    }

    /**
     * @param string $type
     * @param string $message
     * @return void
     */
    public function setFlash(string $type, string $message): void
    {
        $_SESSION[$type] = $message;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getFlash(string $type): string
    {
        $message = $_SESSION[$type];
        unset($_SESSION[$type]);
        return $message;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function hasFlash(string $type): bool
    {
        return isset($_SESSION[$type]);
    }

    /**
     * @param UserInterface $user
     * @return void
     */
    public function login(UserInterface $user): void
    {
        $_SESSION["user_id"] = $user->getId();
        $_SESSION["user_name"] = $user->getName();
    }

    /**
     * @return array
     */
    public function getUser(): array
    {
        if (isset($_SESSION["user_id"])) {
            return [
                'user_id' => $_SESSION["user_id"] ?? null,
                'user_name' => $_SESSION["user_name"] ?? null,
            ];
        }
        return [];
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        unset($_SESSION["user_id"]);
        unset($_SESSION["user_name"]);
    }
}