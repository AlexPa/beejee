<?php

namespace app;

class Response extends \Pecee\Http\Response
{
    /**
     * @param string $view
     * @param array $data
     * @return void
     */
    public function html(string $view, array $data): void
    {
        $layout = App::getBaseDir() . "views" . DIRECTORY_SEPARATOR . "layout" . DIRECTORY_SEPARATOR . "index.php";
        $content = $this->generateContent($view, $data);
        ob_start();
        include($layout);
        $ret = ob_get_contents();
        ob_end_clean();
        $this->header('Content-Type: text/html; charset=utf-8');
        echo $ret;
        exit(0);
    }

    /**
     * @param string $view
     * @param array $data
     * @return false|string
     */
    private function generateContent(string $view, array $data)
    {
        $file = App::getBaseDir() . "views" . DIRECTORY_SEPARATOR . $view . ".php";
        ob_start();
        include($file);
        $ret = ob_get_contents();
        ob_end_clean();
        return $ret;
    }
}