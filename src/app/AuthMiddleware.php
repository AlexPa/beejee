<?php

namespace app;

use models\User;
use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

class AuthMiddleware implements IMiddleware
{

    /**
     * @param Request $request
     * @return void
     */
    public function handle(Request $request): void
    {
        if (!User::getIsLogged()) {
            Router::response()->redirect('/forbidden');
        }
    }
}