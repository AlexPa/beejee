<?php

namespace app;

use app\formRequest\TaskListRequest;

class Helper
{
    /**
     * @param string $path
     * @param int $page
     * @param TaskListRequest $request
     * @return string
     */
    public static function getPaginationLink(string $path, int $page, TaskListRequest $request): string
    {
        $params = [];
        $params['page'] = $page;
        if ($request->getOrderDirection() == SORT_ASC) {
            $params['order'] = "-" . $request->getOrder();
        } else {
            $params['order'] = $request->getOrder();
        }


        return $path . "?" . http_build_query($params);
    }

    /**
     * @param string $path
     * @param string $field
     * @param TaskListRequest $request
     * @return string
     */
    public static function getSortLink(string $path, string $field, TaskListRequest $request): string
    {
        $currentOrder = $request->getOrder();
        $params = [];

        if ($request->getOrderDirection() == SORT_ASC) {
            if ($currentOrder != $field) {
                $params['order'] = "-" . $field;
            } else {
                $params['order'] = $field;
            }
        } else {
            if ($currentOrder == $field) {
                $params['order'] = "-" . $field;
            } else {
                $params['order'] = $field;
            }
        }
        return $path . "?" . http_build_query($params);
    }
}