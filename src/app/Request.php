<?php

namespace app;

use app\formRequest\ValidateInterface;

class Request extends \Pecee\Http\Request
{
    public const METHOD_GET = "get";
    public const METHOD_POST = "post";

    /**
     * @param string $class
     * @return ValidateInterface
     */
    public static function createObjFromPost(string $class): ValidateInterface
    {
        $cl = new $class();
        if ($cl instanceof ValidateInterface) {
            foreach ($cl->getPropsForValidate() as $prop) {
                $cl->$prop = $_POST[$prop] ?? "";
            }
        }
        return $cl;
    }

    /**
     * @param array $data
     * @param string $class
     * @return ValidateInterface
     */
    public static function createObj(array $data, string $class): ValidateInterface
    {
        $cl = new $class();
        if ($cl instanceof ValidateInterface) {
            foreach ($cl->getPropsForValidate() as $prop) {
                $cl->$prop = $data[$prop] ?? "";
            }
        }
        return $cl;
    }

    /**
     * @param string $class
     * @return ValidateInterface
     */
    public static function createObjFromGet(string $class): ValidateInterface
    {
        $cl = new $class();
        if ($cl instanceof ValidateInterface) {
            foreach ($cl->getPropsForValidate() as $prop) {
                $cl->$prop = $_GET[$prop] ?? "";
            }
        }
        return $cl;
    }
}