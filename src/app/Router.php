<?php

namespace app;

use Pecee\SimpleRouter\SimpleRouter;

class Router extends SimpleRouter
{
    private static Response|null $resp = null;

    /**
     * @return Response
     */
    public static function response(): Response
    {
        if (static::$resp === null) {
            static::$resp = new Response(static::request());
        }

        return static::$resp;
    }
}