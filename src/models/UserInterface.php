<?php

namespace models;

use app\formRequest\LoginRequest;

interface UserInterface
{
    public static function getIsLogged(): bool;

    public static function getLoggedUser(): ?UserInterface;

    public static function isAdmin(): bool;

    public function getUserById(int $id): ?UserInterface;

    public function getUserByEmail(string $email): ?UserInterface;

    public function getUserByName(string $name): ?UserInterface;

    public function createUser(UserInterface $user): bool;

    public function validatePassword(string $password, string $hash): bool;

    public function getId(): int;

    public function getPasswordHash(): string;

    public function getEmail(): string;

    public function getName(): string;

    public function getCreateDate(): string;

    public function getPassword(): string;

    public function login(LoginRequest $request): bool;

    public function getLoginError(): string;
}