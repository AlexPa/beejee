<?php

namespace models;

use app\App;
use app\formRequest\TaskListRequest;
use app\formRequest\TaskRequest;

class Task implements TaskInterface
{
    public const STATUS_NEW = 0;
    public const STATUS_FINISHED = 1;

    public const ADMIN_EDITED_YES = 1;
    public const ADMIN_EDITED_NO = 0;

    public int $id;
    public string $name;
    public string $email;
    public string $text;
    private int $status;
    private string $create_date;

    /**
     * @param TaskRequest $taskRequest
     * @return bool
     */
    public static function createTask(TaskRequest $taskRequest): bool
    {
        $insertId = App::getDb()->getServer()->insert('task', [
            'email' => $taskRequest->email,
            'name' => $taskRequest->name,
            'text' => $taskRequest->text,
            'status' => Task::STATUS_NEW,
            'create_date' => date("Y-m-d H:i:s")
        ]);
        if ($insertId) {
            return true;
        }
        return false;
    }

    /**
     * @param TaskListRequest $request
     * @return array
     */
    public static function getTasks(TaskListRequest $request): array
    {
        $orderDirection = ($request->getOrderDirection() == SORT_ASC) ? "ASC" : "DESC";
        $order = $request->getOrder() . " " . $orderDirection;
        $res = App::getDb()->getServer()->prepare(
            "SELECT `id`, `name`, `email`, `status`, `text`, `create_date`, `admin_edited` FROM `task` ORDER BY $order LIMIT :limit OFFSET :offset"
        );
        $res->bindValue(":limit", $request->getLimit(), \PDO::PARAM_INT);
        $res->bindValue(":offset", $request->getOffset(), \PDO::PARAM_INT);
        $res->execute();
        return $res->fetchAll();
    }

    /**
     * @param TaskListRequest $request
     * @return int
     */
    public static function getTasksCount(TaskListRequest $request): int
    {
        $orderDirection = ($request->getOrderDirection() == SORT_ASC) ? "ASC" : "DESC";
        $order = $request->getOrder() . " " . $orderDirection;
        $res = App::getDb()->getServer()->prepare(
            "SELECT COUNT(*) cnt FROM `task`"
        );
        if ($res->execute()) {
            return $res->fetch()->cnt;
        }
        return 0;
    }

    /**
     * @param int $id
     * @param TaskRequest $taskRequest
     * @return bool
     */
    public static function updateTask(int $id, TaskRequest $taskRequest): bool
    {
        $oldTask = self::getTaskById($id);
        $adminEdited = self::ADMIN_EDITED_NO;
        if ($oldTask->getText() != $taskRequest->text) {
            $adminEdited = self::ADMIN_EDITED_YES;
        }
        $res = App::getDb()->getServer()->prepare(
            "UPDATE `task` SET `name`=:name, `email`=:email, `text`=:text, `status`=:status, `admin_edited`=:admin_edited WHERE id=:id"
        );
        $res->bindValue(":name", $taskRequest->name, \PDO::PARAM_STR);
        $res->bindValue(":email", $taskRequest->email, \PDO::PARAM_STR);
        $res->bindValue(":text", $taskRequest->text, \PDO::PARAM_STR);
        $res->bindValue(":status", $taskRequest->status, \PDO::PARAM_STR);
        $res->bindValue(":admin_edited", $adminEdited, \PDO::PARAM_STR);

        $res->bindValue(":id", $id, \PDO::PARAM_INT);
        if ($res->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * @return TaskInterface|null
     */
    public static function getTaskById(int $id): ?TaskInterface
    {
        $res = App::getDb()->getServer()->prepare(
            "SELECT `id`, `name`, `email`, `status`, `text`, `create_date`, `admin_edited` FROM `task` WHERE id=:id"
        );
        $res->bindValue(":id", $id, \PDO::PARAM_INT);
        $res->execute();
        if (($t = $res->fetch()) != null) {
            $task = new Task();
            $task->id = $t->id;
            $task->name = $t->name;
            $task->email = $t->email;
            $task->text = $t->text;
            $task->status = $t->status;
            $task->admin_edited = $t->admin_edited;
            $task->create_date = $t->create_date;
            return $task;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getCreateDate(): string
    {
        return $this->create_date;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}