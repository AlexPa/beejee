<?php

namespace models;

use app\App;
use app\formRequest\LoginRequest;

class User implements UserInterface
{
    public string $email;
    public string $name;
    public string $password;
    private string $create_date;
    private string $password_hash;
    private int $id;

    private string $loginError = "";

    /**
     * @return bool
     */
    public static function getIsLogged(): bool
    {
        return (self::getLoggedUser() != null);
    }

    /**
     * @return UserInterface|null
     */
    public static function getLoggedUser(): ?UserInterface
    {
        $user = App::getSession()->getUser();
        if (empty($user)) {
            return null;
        }
        $u = new User();
        $u->id = $user['user_id'];
        $u->name = $user['user_name'];
        return $u;
    }

    /**
     * @return bool
     */
    public static function isAdmin(): bool
    {
        $user = self::getLoggedUser();
        if ($user != null) {
            if ($user->getName() == "admin") {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->password_hash;
    }

    /**
     * @return string
     */
    public function getCreateDate(): string
    {
        return $this->create_date;
    }

    /**
     * @param int $id
     * @return UserInterface|null
     */
    public function getUserById(int $id): ?UserInterface
    {
        $res = App::getDb()->getServer()->prepare(
            'SELECT id, name, email, password_hash, create_date FROM user WHERE id=? LIMIT 1'
        );
        $res->execute([$id]);
        $user = $res->fetch();
        if ($user === false) {
            return null;
        }
        return $this->makeUser($user);
    }

    /**
     * @param \stdClass $u
     * @return UserInterface
     */
    private function makeUser(\stdClass $u): UserInterface
    {
        $user = new User();
        $user->name = $u->name;
        $user->email = $u->email;
        $user->password_hash = $u->password_hash;
        $user->id = $u->id;
        $user->create_date = $u->create_date;
        return $user;
    }

    /**
     * @param string $email
     * @return UserInterface|null
     */
    public function getUserByEmail(string $email): ?UserInterface
    {
        $res = App::getDb()->getServer()->prepare(
            'SELECT id, name, email, password_hash, create_date FROM user WHERE email=? LIMIT 1'
        );
        $res->execute([$email]);
        $user = $res->fetch();
        if ($user === false) {
            return null;
        }
        return $this->makeUser($user);
    }

    /**
     * @param LoginRequest $request
     * @return bool
     */
    public function login(LoginRequest $request): bool
    {
        $res = App::getDb()->getServer()->prepare('SELECT password_hash FROM user WHERE name=? LIMIT 1');
        $res->execute([$request->name]);
        $user = $res->fetch();
        if ($user === false) {
            $this->loginError = "Пользователь не существует";
            return false;
        }
        if ($this->validatePassword($request->password, $user->password_hash)) {
            return true;
        } else {
            $this->loginError = "Не верный пароль";
        }
        return false;
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function validatePassword(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * @return string
     */
    public function getLoginError(): string
    {
        return $this->loginError;
    }

    /**
     * @param string $name
     * @return UserInterface|null
     */
    public function getUserByName(string $name): ?UserInterface
    {
        $res = App::getDb()->getServer()->prepare(
            'SELECT id, name, email, password_hash, create_date FROM user WHERE name=? LIMIT 1'
        );
        $res->execute([$name]);
        $user = $res->fetch();
        if ($user === false) {
            return null;
        }
        return $this->makeUser($user);
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function createUser(UserInterface $user): bool
    {
        $insertId = App::getDb()->getServer()->insert('user', [
            'email' => $user->getEmail(),
            'name' => $user->getName(),
            'password_hash' => password_hash($user->getPassword(), PASSWORD_BCRYPT),
            'create_date' => date("Y-m-d H:i:s")
        ]);
        if ($insertId) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}