<?php

namespace models;

use app\formRequest\TaskListRequest;
use app\formRequest\TaskRequest;

interface TaskInterface
{
    public static function createTask(TaskRequest $taskRequest): bool;

    public static function updateTask(int $id, TaskRequest $taskRequest): bool;

    public static function getTaskById(int $id): ?TaskInterface;

    public static function getTasks(TaskListRequest $request): array;

    public static function getTasksCount(TaskListRequest $request): int;

    public function getId(): int;

    public function getName(): string;

    public function getEmail(): string;


    public function getText(): string;

    public function getStatus(): int;

    public function getCreateDate(): string;
}