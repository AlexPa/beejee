<?php

return [
    'components' => [
        'db' => [
            'port' => '3306',
            'hostname' => 'mysql',
            'database' => 'bee_jee',
            'username' => 'root',
            'password' => 'password',
            'charset' => 'utf8'
        ]
    ]
];