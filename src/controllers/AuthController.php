<?php

namespace controllers;

use app\App;
use app\formRequest\LoginRequest;
use app\Request;
use app\Router;
use app\Session;
use models\User;
use models\UserInterface;

class AuthController extends BaseController
{
    /**
     * @return void
     */
    public function logout(): void
    {
        App::getSession()->logout();
        App::getSession()->setFlash(Session::FLASH_SUCCESS, "Вы вышли");
        Router::response()->redirect("/");
    }

    /**
     * @return void
     */
    public function login(): void
    {
        $request = Router::request();
        $loginRequest = Request::createObjFromPost(LoginRequest::class);

        if ($request->getMethod() == Request::METHOD_POST) {
            $user = new User();
            if ($loginRequest->validate() && $user->login($loginRequest)) {
                /**
                 * @var $u UserInterface
                 */
                $u = $user->getUserByName($loginRequest->name);

                App::getSession()->login($u);
                App::getSession()->setFlash(Session::FLASH_SUCCESS, "Вы вошли как {$u->getName()}");
                Router::response()->redirect("/");
            }
            $errors = $loginRequest->getErrors();
            if (($e = $user->getLoginError()) != "") {
                $errors[] = $e;
            }
            App::getSession()->setFlash(Session::FLASH_ERROR, implode("<br />", $errors));
            $this->renderView($loginRequest);
        }
        if ($request->getMethod() == Request::METHOD_GET) {
            $this->renderView($loginRequest);
        }
    }

    /**
     * @param LoginRequest $loginRequest
     * @return void
     */
    private function renderView(LoginRequest $loginRequest)
    {
        Router::response()->httpCode(200)->html("auth/login", ['loginRequest' => $loginRequest]);
    }
}