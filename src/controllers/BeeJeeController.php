<?php

namespace controllers;

use app\App;
use app\formRequest\TaskListRequest;
use app\formRequest\TaskRequest;
use app\Request;
use app\Router;
use app\Session;
use models\Task;

class BeeJeeController extends BaseController
{

    /**
     * @return void
     */
    public function index(): void
    {
        $taskRequest = Request::createObjFromGet(TaskListRequest::class);
        $taskRequest->init();

        $tasks = Task::getTasks($taskRequest);
        $tasksCount = Task::getTasksCount($taskRequest);

        Router::response()->httpCode(200)->html("beejee/index", [
            'taskRequest' => $taskRequest,
            'tasks' => $tasks,
            'tasksCount' => $tasksCount
        ]);
    }

    /**
     * @param int $id
     * @return void
     */
    public function updateTask(int $id): void
    {
        $task = Task::getTaskById($id);
        if ($task == null) {
            Router::response()->redirect('/notfound');
        }
        $request = Router::request();
        $taskRequest = Request::createObj([
            'name' => $task->getName(),
            'email' => $task->getEmail(),
            'text' => $task->getText(),
            'status' => $task->getStatus()
        ], TaskRequest::class);

        if ($request->getMethod() == Request::METHOD_POST) {
            $taskRequest = Request::createObj([
                'name' => $_POST["name"] ?? $task->getName(),
                'email' => $_POST["email"] ?? $task->getEmail(),
                'text' => $_POST["text"] ?? $task->getText(),
                'status' => $_POST["status"] ?? Task::STATUS_NEW
            ], TaskRequest::class);

            if ($taskRequest->validate() && Task::updateTask($task->getId(), $taskRequest)) {
                App::getSession()->setFlash(Session::FLASH_SUCCESS, "Задача успешно обновлена");
                Router::response()->redirect("/");
            }
            $errors = $taskRequest->getErrors();
            App::getSession()->setFlash(Session::FLASH_ERROR, implode("<br />", $errors));
            Router::response()->httpCode(200)->html("beejee/update_task", [
                'taskRequest' => $taskRequest,
                'task' => $task,
            ]);
        }
        if ($request->getMethod() == Request::METHOD_GET) {
            Router::response()->httpCode(200)->html("beejee/update_task", [
                'taskRequest' => $taskRequest,
                'task' => $task,
            ]);
        }
    }

    /**
     * @return void
     */
    public function createTask(): void
    {
        $request = Router::request();
        $taskRequest = Request::createObj([
            'name' => $_POST["name"] ?? "",
            'email' => $_POST["email"] ?? "",
            'text' => $_POST["text"] ?? "",
            'status' => $_POST["status"] ?? Task::STATUS_NEW
        ], TaskRequest::class);

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($taskRequest->validate() && Task::createTask($taskRequest)) {
                App::getSession()->setFlash(Session::FLASH_SUCCESS, "Задача успешно создана");
                Router::response()->redirect("/");
            }
            $errors = $taskRequest->getErrors();
            App::getSession()->setFlash(Session::FLASH_ERROR, implode("<br />", $errors));
            Router::response()->httpCode(200)->html("beejee/create_task", [
                'taskRequest' => $taskRequest
            ]);
        }
        if ($request->getMethod() == Request::METHOD_GET) {
            Router::response()->httpCode(200)->html("beejee/create_task", [
                'taskRequest' => $taskRequest,
                'task' => null,
            ]);
        }
    }
}