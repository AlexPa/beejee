<?php

namespace controllers;

use app\Router;

class DefaultController extends BaseController
{
    /**
     * @return void
     */
    public function notfound()
    {
        Router::response()->httpCode(404)->html("default/notfound", []);
    }

    /**
     * @return void
     */
    public function forbidden()
    {
        Router::response()->httpCode(403)->html("default/forbidden", []);
    }
}