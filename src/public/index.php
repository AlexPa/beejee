<?php

error_reporting(E_ALL);
const DEBUG = false;
require __DIR__ . '/../vendor/autoload.php';


$config = array_merge(
    require __DIR__ . '/../config/common.php',
    require __DIR__ . '/../config/db.php',
);

(new \app\App($config))->run();


