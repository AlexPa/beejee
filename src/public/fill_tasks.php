<?php

error_reporting(E_ALL);
const DEBUG = false;
require __DIR__ . '/../vendor/autoload.php';


$config = array_merge(
    require __DIR__ . '/../config/common.php',
    require __DIR__ . '/../config/db.php',
);

$app = (new \app\App($config));

$faker = Faker\Factory::create();
for ($i = 0; $i < 10; $i++) {
    $task = new \models\Task();
    $task->name = $faker->sentence();
    $task->email = $faker->email;
    $task->name = $faker->name;
    $task->text = $faker->text(300);
    \models\Task::createTask($task);
}
echo "Work done\n";

