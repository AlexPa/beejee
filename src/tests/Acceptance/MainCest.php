<?php


namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

class MainCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function pagesNeededAuthNotOpensTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/task/update/1000000');
        $I->canSeeResponseCodeIs(403);
    }

    public function pagesAreOpensTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/');
        $I->see('Список задач');

        $I->amOnPage('/task/create');
        $I->see('Создать задачу');

        $I->amOnPage('/auth/login');
        $I->see('Войти');
    }

    public function loginWorksTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/auth/login');
        $I->cantSee('admin');
        $I->submitForm('#loginForm', [
            'name' => "admin",
            'password' => "123"
        ]);
        $I->see('admin');

        $I->amOnPage('/task/update/1000000');
        $I->canSeeResponseCodeIs(404);
    }
}
