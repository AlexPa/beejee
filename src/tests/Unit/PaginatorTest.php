<?php


namespace Tests\Unit;

use app\formRequest\TaskListRequest;
use app\Helper;
use Tests\Support\UnitTester;

class PaginatorTest extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    public function testPaginator()
    {
        $r = new TaskListRequest();
        $l = Helper::getPaginationLink("/", 1, $r);
        $this->assertEquals($l, "/?page=1&order=id");

        $r->page = 2;
        $sortL = Helper::getSortLink("/", "email", $r);
        $this->assertEquals($sortL, "/?order=email");
        $sortL = Helper::getSortLink("/", "-id", $r);
        $this->assertEquals($sortL, "/?order=-id");


        $r->order = "email";
        $l = Helper::getPaginationLink("/", 1, $r);
        $this->assertEquals($l, "/?page=1&order=email");


        $r = new TaskListRequest();
        $r->order = "TryToSqlInject";
        $this->assertEquals($r->getOrder(), "TryToSqlInject");

        $r->init();
        $this->assertEquals($r->getOrder(), "id");
    }

    protected function _before()
    {
    }
}
